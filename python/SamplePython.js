from models import TagsModel
from models import KeywordAliases

import re

class QuestionTagger(object):
    """QuestionTagger: """
    def __init__(self):
        super(QuestionTagger, self).__init__()
        tm = TagsModel.TagsModel()
        self.cleaned_tags_list = tm.get_cleaned_tags_list()

        ka = KeywordAliases.KeywordAliases()
        self.relevant_keyword_aliases_final = ka.get_all_keyword_aliases()

    def convert_all_aliases_to_leader(self, text):
        for leader, aliases in self.relevant_keyword_aliases_final.items():
            for alias in aliases:
                text = re.sub('([^A-Za-z])' + alias + '([^A-Za-z])', '\g<1>' + leader + '\g<2>', text)
                text = re.sub('(^)' + alias + '([^A-Za-z])', '\g<1>' + leader + '\g<2>', text)
                text = re.sub('([^A-Za-z])' + alias + '($)', '\g<1>' + leader + '\g<2>', text)
        return text

    def find_question_tags_strong_match_new(self, question_text):
        matching_tags = []
        cleaned_question_text = self.convert_all_aliases_to_leader(question_text)
        for main_tag, tag_aliases in self.cleaned_tags_list.items():
            tags = [main_tag]
            if tag_aliases.strip() != '':
                tag_aliases = tag_aliases.split(',')
                for tag_alias in tag_aliases:
                    tag_alias = tag_alias.strip()
                    if tag_alias != '':
                        tags.append(tag_alias)

            for tag in tags:
                matched_count = 0
                tag_parts = tag.split()
                for tag_part in tag_parts:
                    if tag_part.strip() in ['your', 'yours', 'for', 'a']:
                        continue

                    if (tag_part in cleaned_question_text):
                        matched_count += 1
                        continue

                    if (((tag_part + 's') in cleaned_question_text)):
                        matched_count += 1
                        continue

                    if ((tag_part[-1] == 's') and (tag_part[0:-1] in cleaned_question_text)):
                        matched_count += 1
                        continue

                if float(matched_count)/float(len(tag_parts)) > 0.5:
                    matching_tags.append(main_tag)
                    break
        return matching_tags