package cibola.co.in.General;

import java.io.File;
import java.util.Locale;
import java.util.Random;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cibola.co.in.Graphics.AlphabetImage;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.UI.R;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class Common {

	private static String FONT_HIGHLIGHTED = "<font color='#5DB3D9'>";
	private static String FONT_CLOSE = "</font>";	
	
	public static ContentValues createContactValues(String cid, String cName, long fileID, String filePath){
		ContentValues cv = new ContentValues();
		cv.put(Tables.contact_name, cName);
		cv.put(Tables.contact_mobile, cid);
		cv.put(Tables.contact_background, AlphabetImage.palatte[new Random().nextInt(AlphabetImage.palatte.length)]);
		cv.put(Tables.contact_image_id, fileID);
		cv.put(Tables.contact_image_local_filepath, filePath);
		cv.put(Tables.contact_timestamp, (new java.util.Date()).getTime());
		return cv;
	}
	
	public static ContentValues createRawUpdateValues(String opp_id,String name, String email, int ack){
		ContentValues cv = new ContentValues();
		cv.put(Tables.rawupdates_mobile, opp_id);
		cv.put(Tables.rawupdates_name,name);
		cv.put(Tables.rawupdates_email, email);
		cv.put(Tables.rawupdates_ack, ack);
		cv.put(Tables.rawupdates_timestamp, (new java.util.Date()).getTime());
		return cv;
	}
	
	// Create Content Values for transaction table. 
	public static ContentValues createCVtransaction(String transaction_id, String opp_id, 
			int to_from, int send_req, int amount, String comment,int isDate, long timestamp){
		ContentValues cv = new ContentValues();
		cv.put(Tables.trans_transaction_id, transaction_id);
		cv.put(Tables.trans_opponent_id, opp_id);
		cv.put(Tables.trans_to_from, to_from);
		cv.put(Tables.trans_send_req, send_req);
		cv.put(Tables.trans_amount, amount);
		if(comment!=null){
			cv.put(Tables.trans_comment, comment);
		}
		cv.put(Tables.trans_timestamp, timestamp); 	
		cv.put(Tables.trans_isDate, 0);
		cv.put(Tables.trans_status, 0);
		return cv;
	}
	
	public static void setTextViewFontStyle(String font, AssetManager asset,TextView...params ){
		Typeface tf = Typeface.createFromAsset(asset, font);
		for(TextView v: params){
			v.setTypeface(tf);
		}
	}
	
	public static void setEditTextFontStyle(String font, AssetManager asset,EditText...params ){
		Typeface tf = Typeface.createFromAsset(asset, Constants.FONT_ROBOTO_REGULAR);
		for(EditText v: params){
			v.setTypeface(tf);
		}
	}
		
	public static String generateTransactionId(){
		String tid = "CIBOLA";
		tid += (new java.util.Date()).getTime();
		Random rand = new Random();
	    int randomNum = rand.nextInt(8999)+1000 ; // generate a random 4 digit number
	    tid+=randomNum;
	    return tid;
	}
	
	public static void hideKeyBoardFromEditText(EditText editText, Context context){
		InputMethodManager imm = (InputMethodManager) context.getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}
	
	public static void showSoftInputKeyboard(Context context, EditText editText){
		InputMethodManager imm =  (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInputFromWindow(editText.getApplicationWindowToken(),InputMethodManager.SHOW_FORCED, 0);
	}

	public static ProgressDialog getProgressDialog(Context context, String message,boolean cancelable){
		ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setIndeterminate(true);
		pDialog.setCancelable(cancelable);
		pDialog.setMessage(message);
		return pDialog;
	}
	
	public static void showToast(Context context, String message){
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	
	public static int getDrawable(String s){
		char c = (s.charAt(0)+"").toUpperCase(Locale.US).charAt(0); 
			switch (c) {
			case 'A':
				return R.drawable.a;
			case 'B':
				return R.drawable.b;
			case 'C':
				return R.drawable.c;
			case 'D':
				return R.drawable.d;
			case 'E':
				return R.drawable.e;
			case 'F':
				return R.drawable.f;
			case 'G':
				return R.drawable.g;
			case 'H':
				return R.drawable.h;
			case 'I':
				return R.drawable.i;
			case 'J':
				return R.drawable.j;
			case 'K':
				return R.drawable.k;
			case 'L':
				return R.drawable.l;
			case 'M':
				return R.drawable.m;
			case 'N':
				return R.drawable.n;
			case 'O':
				return R.drawable.o;
			case 'P':
				return R.drawable.p;
			case 'Q':
				return R.drawable.q;
			case 'R':
				return R.drawable.r;
			case 'S':
				return R.drawable.s;
			case 'T':
				return R.drawable.t;
			case 'U':
				return R.drawable.u;
			case 'V':
				return R.drawable.v;
			case 'W':
				return R.drawable.w;
			case 'X':
				return R.drawable.x;
			case 'Y':
				return R.drawable.y;
			case 'Z':
				return R.drawable.z;
			default:
				//Log.d("DEBUG","NO Char match: "+c);
				return -1;
			}
		
	}

	public void getGCMRegID(final Context context){
		AsyncTask< Void, Void, String> asyncTask = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				String regID="";
				try{
					GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
					regID = gcm.register(Constants.GCM_SENDER_ID);
					//Log.d(Constants.DEBUG,"Received registration id:-"+regID);
				}
				catch(Exception e){
					e.printStackTrace();
				}
				return regID;
			}
			
			@Override
			protected void onPostExecute(String regID){
				MyAccountManager accountManager = new MyAccountManager(context);
				if(regID!=null && regID.length()>0){
					accountManager.setHasGCMId(accountManager.setGCMId(regID));
				}
			}
		};
		asyncTask.execute();

	}

	public static void setupActionBar(ActionBar actionBar){
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setHomeAsUpIndicator(R.drawable.custom_icon_back);
		actionBar.setIcon(R.drawable.ic_launcher);
	} 
	
	public static String getContactImagePath(String contactNumber, ImageType type){
		switch (type) {
		case Large:
			return Environment.getExternalStorageDirectory()+Constants.CIBOLA_LARGE_IMAGE_DIR+"/"+contactNumber+".png";
		case Medium:
			return Environment.getExternalStorageDirectory()+Constants.CIBOLA_MEDIUM_IMAGE_DIR+"/"+contactNumber+".png";
		case Self:
			return Environment.getExternalStorageDirectory()+Constants.CIBOLA_MAIN_IMAGE_DIR+"/profile.png";
		default:
			return null;
		}
	}
	
	public static void deleteLargeImage(String contactNumber){
		File file = new File(Common.getContactImagePath(contactNumber, ImageType.Large));
		 if(file.exists()){
			 file.delete();
		 }
	}

	public static boolean isImageExistsOnDisk(String contactNumber, ImageType type){
		File file = new File(getContactImagePath(contactNumber, type));
		if(file.exists())
			return true;
		return false;
	}
	
	
	
	public static String getSearchHighlightedString(String mCurFilter, String fullname){
		
		if(mCurFilter==null || mCurFilter.length()==0){
			return fullname;
		}
		else{
			int len = mCurFilter.length();
			if(mCurFilter.contains(" ")){
				if(len<=fullname.length()){
					String output = "";
					String tmp = fullname.substring(0,len);
					if(tmp.toLowerCase(Locale.getDefault()).equals(mCurFilter.toLowerCase(Locale.getDefault()))){
						output+= FONT_HIGHLIGHTED+tmp+FONT_CLOSE;
						output+= fullname.substring(len);
					}
					else{
						output = fullname;
					}
					return output;
				}
				else{
					return fullname;
				}
			}
			String[] name_arr = fullname.split(" ");
			String output = "";
			
			for(int i=0;i<name_arr.length;i++){
				if(len<=name_arr[i].length()){
					String tmp = name_arr[i].substring(0,len);
					if((tmp.toLowerCase(Locale.getDefault()).equals(mCurFilter.toLowerCase(Locale.getDefault())))){
						output+= FONT_HIGHLIGHTED+tmp+FONT_CLOSE;
						output+= name_arr[i].substring(len);
					}
					else{
						output+= name_arr[i];
					}
					if(i!=name_arr.length-1){
						output+=" ";
					}
				}
				else{
					output+= (name_arr[i]);
					if(i<name_arr.length-1){
						output+=" ";
					}
				}
			}
			return output;
		}
	}

	public static String getCommaSepString(String str){
		int len = str.length();
		if(len<=3){
			return str;
		}
		else if(len==4){
			return str.substring(0,1)+","+str.substring(1);
		}
		else if( len==5){
			return str.substring(0,2)+","+str.substring(2);
		} 
		else if(len==6){
			return str.substring(0,1)+","+str.substring(1,3)+","+str.substring(3);
		} 
		else{
			return str;
		}
		
	}
	
	
	public static String getAutoCapString(String str){
		if(str.length()>1){
			return Character.toUpperCase(str.charAt(0)) + str.substring(1);
		}
		return str;
	}

	public static View getCustomNoteView(Context context, LayoutInflater inflater, String title, String description, int icon, 
			int bgColor, OnClickListener onClickListener){
		final View view = inflater.inflate(R.layout.custom_note, null);
		TextView titleView = (TextView) view.findViewById(R.id.custom_note_title);
		TextView descriptionView = (TextView) view.findViewById(R.id.custom_note_description);
		ImageView iconView = (ImageView) view.findViewById(R.id.custom_note_icon);
		ImageView close = (ImageView) view.findViewById(R.id.custom_note_close);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_MEDIUM, context.getAssets(), titleView);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, context.getAssets(),  descriptionView);
		titleView.setText(title);
		descriptionView.setText(Html.fromHtml(description));
		iconView.setImageResource(icon);
		view.setBackgroundColor(bgColor);
		close.setOnClickListener(onClickListener);
		return view;
	}
	
	
}
