const Bluebird   = require('bluebird');
const Freshdesk = require('freshdesk-api');
const request = require('request');
const logger = require('log4js').getLogger();

// FreshService APIs
const REQUESTER_URI = "/api/v2/requesters";
const REQUESTER_FIELDS_URI = "/api/v2/requester_fields";
const AGENT_URI = "/api/v2/agents";
const GROUP_URI = "/api/v2/groups";

let FreshService = {

	getAsyncFresh: function(server, apiKey){
		let asyncFreshConn = Bluebird.promisifyAll(
		    new Freshdesk(server, apiKey)
		);
		return asyncFreshConn;
	},

	getAuthHeaders: function(apiKey){
		let auth = "Basic " + Buffer.from(apiKey + ":" + 'X').toString("base64");
		return auth;
	},

	makeRequest: function (method, auth, url, qs, data) {		// eslint-disable-line max-params
		return new Promise(function(resolve, reject){
			const options = {
				method: method,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': auth
				},
				url: url,
				qs: qs
			}
			if(data) {
				options.body = JSON.stringify(data)
			}
			logger.info("Freshservice URL: ", options.method, options.url, data);
			request(options, async function(error, response, body){
				if (error) {
					return reject(error);
				}
				if(response.statusCode >= 200 && response.statusCode <=400){
					let res = body ? JSON.parse(body) : null;
					return resolve(res);
				}else{
					logger.error("FreshService error occurred: ", error, body);
          return reject(
            {
              "name": "FreshServiceError",
              "statusCode": response.statusCode,
              "message": response.statusMessage
            }
          );
				}
			})
		});
	},

	getRequesterById: async function(freshClient, id){
		let requester = await this.makeRequest('GET', freshClient._auth, `${freshClient.baseUrl}${REQUESTER_URI}/${id}`);
		if(requester){
			return requester.requester; //requester.requester;
		}
		return null;
	},

	getRequesterListByEmail: async function(freshClient, email, startIndex, count){
		let url = `${freshClient.baseUrl}${REQUESTER_URI}?email=${email}`;
		if((startIndex || startIndex == 0) && count){
			url = `${url}&page=${startIndex}&per_page=${count}`
		}
		let requesters = await this.makeRequest('GET', freshClient._auth, url);
		return requesters.requesters; //requesters; ;
	},

	getRequesterList: async function(freshClient, startIndex, count){
		let url = `${freshClient.baseUrl}${REQUESTER_URI}`;
		if((startIndex || startIndex == 0) && count){
			url = `${url}?page=${startIndex}&per_page=${count}`
		}
		let requesters = await this.makeRequest('GET', freshClient._auth, url);
		return requesters.requesters;
	},

	createRequester: async function(freshClient, payload){
		logger.info("Create requester payload:: ", payload);
		let requester = await this.makeRequest('POST', freshClient._auth, `${freshClient.baseUrl}${REQUESTER_URI}`, null, payload);
		if(requester){
			return requester.requester;
		}
		return null;
	},

	createRequesterIfNotExist: async function(freshClient, name, email, mobile){
		let requester = null;
		if(name && email && mobile){
			requester = await FreshService.getRequesterByEmail(freshClient, email);
			if(!requester){
				requester = await FreshService.createRequester(freshClient, name, email, mobile);
			}
		}
		if(requester){
			return requester.requester;
		}
		return null;
	},

	updateRequester: async function(freshClient, id, updateObj){
		logger.info("UpdateObj: ", updateObj);
		let requester = await this.makeRequest('PUT', freshClient._auth, `${freshClient.baseUrl}${REQUESTER_URI}/${id}`, null, updateObj);
		if(requester){
			return requester.requester;
		}
		return null;
	},

	deleteRequester: async function(freshClient, id){
		let requester = await this.makeRequest('DELETE', freshClient._auth, `${freshClient.baseUrl}${REQUESTER_URI}/${id}`);
		logger.debug("deleted user", requester);
		return requester.requester;
	},

	getRequesterFields: async function(freshClient){
		let url = `${freshClient.baseUrl}${REQUESTER_FIELDS_URI}`;
		let fields = await this.makeRequest('GET', freshClient._auth, url);
		return fields.requester_fields; 
	},

	makeAgent: async function(freshClient, id){
		// let payload = {"occasional": true};
		let payload = {};
		let agent = await this.makeRequest('PUT', freshClient._auth, `${freshClient.baseUrl}${REQUESTER_URI}/${id}/make_agent`, null, payload);
		if(agent){
			return agent.agent;
		}
		return null;
	},

	getAgentById: async function(freshClient, id){
		try{
			let agent = await this.makeRequest('GET', freshClient._auth, `${freshClient.baseUrl}${AGENT_URI}/${id}`);
			if(agent){
				return agent.agent;
			}
		}catch(err){
			logger.error("Error occurred while fetching agent", err);
		}
		return null;
	},
	
	removeAgentById: async function(freshClient, id){
		try{
			await this.makeRequest('DELETE', freshClient._auth, `${freshClient.baseUrl}${AGENT_URI}/${id}`);
			return true;
		}catch(err){
			logger.error("Error occurred while deleting agent", err);
		}
		return false;
	},

	getAgentListByEmail: async function(freshClient, email, startIndex, count){
		let url = `${freshClient.baseUrl}${AGENT_URI}?email=${email}`;
		if((startIndex || startIndex == 0) && count){
			url = `${url}&page=${startIndex}&per_page=${count}`
		}
		let agents = await this.makeRequest('GET', freshClient._auth, url);
		return agents.agents; //requesters.requesters;
	},

	updateAgent: async function(freshClient, id, updateObj){
		logger.info("updateAgent payload: ", updateObj);
		let agent = await this.makeRequest('PUT', freshClient._auth, `${freshClient.baseUrl}${AGENT_URI}/${id}`, null, updateObj);
		if(agent){
			return agent.agent; //requester.requester;
		}
		return null;
	},

	getAgentList: async function(freshClient, startIndex, count){
		let url = `${freshClient.baseUrl}${AGENT_URI}`;
		if((startIndex || startIndex == 0) && count){
			url = `${url}?page=${startIndex}&per_page=${count}`
		}
		let agents = await this.makeRequest('GET', freshClient._auth, url);
		return agents.agents; //requesters.requesters;
	},

	createGroup: async function(freshClient, payload){
		logger.info("Fresh Create group payload:: ", payload);
		let group = await this.makeRequest('POST', freshClient._auth, `${freshClient.baseUrl}${GROUP_URI}`, null, payload);
		if(group){
			return group.group; 
		}
		return null;
	},

	updateGroup: async function(freshClient, id, updateObj){
		logger.info("UpdateObj: ", updateObj);
		let group = await this.makeRequest('PUT', freshClient._auth, `${freshClient.baseUrl}${GROUP_URI}/${id}`, null, updateObj);
		if(group){
			return group.group; 
		}
		return null;
	},

	getGroupById: async function(freshClient, id){
		let group = await this.makeRequest('GET', freshClient._auth, `${freshClient.baseUrl}${GROUP_URI}/${id}`);
		if(group){
			return group.group; //requester.requester;
		}
		return null;
	},

	getGroupList: async function(freshClient, startIndex, count){
		let url = `${freshClient.baseUrl}${GROUP_URI}`;
		if((startIndex || startIndex == 0) && count){
			url = `${url}?page=${startIndex}&per_page=${count}`
		}
		let groups = await this.makeRequest('GET', freshClient._auth, url);
		if(groups){
			return groups.groups;
		}
		return []; 
	},


}

module.exports = FreshService;
