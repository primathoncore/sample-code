import { Component, OnInit,OnDestroy, Output, EventEmitter,Input } from '@angular/core';
import {DashboardService} from '../shared/service/dashboard.service'
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducers';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import ClevertapReact from 'clevertap-react';
import { CleverTapCommonService } from '../../shared/service/clever-tap-common.service';
import { PreJeeMainService } from '../../shared/service/pre-jee-main.service';
import * as CartActions from '../../store/my-cart/my-cart.actions';

@Component({
  selector: 'dashboard-live-tests',
  templateUrl: './live-tests.component.html',
  styleUrls: ['./live-tests.component.css']
})
export class LiveTestsComponent implements OnInit,OnDestroy {
  
  public showTests : boolean = false;
  public liveTests : any;
  public cardColors : any;
  public showLeftNav : boolean = false;
  public showRightNav : boolean = false;
  public carouselNavigationStyle : any;
  public selectedNavIndex = -1;
  public userId = '';
  public classId = '';
  sub;
  subSubscription: Subscription
    
  constructor(private dashboardService:DashboardService,
    private preJeeService: PreJeeMainService,
    private router: Router,
    private store: Store<AppState>, private cleverTap: CleverTapCommonService) {
  }
  

  ngOnInit() {
    this.showTests = true;
    this.cardColors = [
      'linear-gradient(283.48deg, #5AFCFC -24.19%, #1D7DEA 67.88%)', 
      'linear-gradient(290.23deg, #F77DBA -10.1%, #B06DDE 86.65%)', 
      'linear-gradient(276.84deg, #FCEB5A -24.19%, #FF964A 67.88%)'
    ];    
    this.sub = this.store.select('userProfile').map((data) => {
        return data;
    });

    this.subSubscription = this.sub.subscribe(res => {
      if(!this.userId && res.userData.userId) {
        this.userId = res.userData.userId; 
        this.classId = res.userData.userClass.classId;
        this.getLiveTests(this.userId, this.classId);
      }
    });    
  }
  getLiveTests(userId, classId){
    this.showTests = false;
    this.dashboardService.getLiveTests(userId, classId).subscribe((res)=>{
        if(res.code==200 && res.data && res.data.length){
            this.liveTests = this.getTestsData(res.data);
            this.showTests = true;
            this.selectedNavIndex = 0;
            if(this.liveTests.length>1) {
              this.showRightNav = true;      
            }
        }
    })
 }

 getTestsData(tests) {
  return tests.map((item, index)=>{
    let prev = item.myTest.prev ? this.createTestObject(item.myTest.prev) : null;
    let next = item.myTest.next ? this.createTestObject(item.myTest.next) : null;
    let cardCase = this.createCardCase(item.myTest);
    let testData = {
      prev: prev,
      next: next,
      ...cardCase,
      color: this.cardColors[index%3],
      serverTime: item.serverTime
    }
    return testData;
  })
 } 

 createTestObject(test) {
    let isResume = test.attempts.length > 0 && !test.attempts[0].endTime;
    let attempts = this.createTestAttempt(test)
    return {
      'courseId': test.courseId,
      'testId': test._id,
      'testName': test.testName,
      'startDate': new Date(test.testDate),
      'endDate': new Date(test.endDate),
      'isAttempted': test.attempted,
      'attempts': attempts,
      'status': test.availability.toLowerCase(),
      'syllabus': test.syllabus,
      'duration': test.duration,
      'isBought': test.isBought,
      'price': test.price,
      'packageId': test.packageId,
      'isResume': isResume,
      'classObj': { class: test.class, classId: test.classId },
      'resultAfter': test.resultAfter,
      'isInterested': test.isInterested
    }
 }

 createCardCase(item) {
    let cardCase = {
      'isLast': false,
      'isNext': false,
      'isLastResultPending': false,
      'isNextLive': false,
      'isPreJee': false,
      'isPreJeeAdv': false,
      'isPreBitsat': false,
      'isPreNtse': false,
      isMoreDetailsSecondaryBtn: false,
      testSeries: ''
    }
    cardCase.isPreJeeAdv = (item.prev && item.prev.testName && item.prev.testName.indexOf('JEEA')!==-1) || (item.next && item.next.testName && item.next.testName.indexOf('JEEA')!==-1);
    cardCase.isPreJee = !cardCase.isPreJeeAdv && ((item.prev && item.prev.testName && item.prev.testName.indexOf('JEE')!==-1) || (item.next && item.next.testName && item.next.testName.indexOf('JEE')!==-1));
    cardCase.isPreBitsat = (item.prev && item.prev.testName && item.prev.testName.indexOf('BITSAT')!==-1) || (item.next && item.next.testName && item.next.testName.indexOf('BITSAT')!==-1);
    cardCase.isPreNtse = (item.prev && item.prev.testName && item.prev.testName.indexOf('NTSE')!==-1) || (item.next && item.next.testName && item.next.testName.indexOf('NTSE')!==-1); 
    
    if(cardCase.isPreJeeAdv) {
      cardCase.testSeries = 'Pre-JEE Advanced Test series';
    }
    else if(cardCase.isPreJee) {
      cardCase.testSeries = 'Pre-JEE Main Test series';
    }
    else if(cardCase.isPreBitsat) {
      cardCase.testSeries = 'Pre-BITSAT Main Test series';
    }
    else if(cardCase.isPreNtse) {
      cardCase.testSeries = 'Pre-NTSE Test series';
    }


    let remainingTime;
    if (item.next) {
      cardCase.isNext = true;
    }

    if (item.prev && item.prev.attempts.length > 0) {
       cardCase.isLast = true;
    }
   
    if (cardCase.isLast && item.prev.attempts.length > 0) {
      let testEndTime = new Date(item.prev.endDate);
      let resultDeclareTime = new Date(testEndTime.setMinutes(testEndTime.getMinutes() + item.prev.resultAfter))
      if (resultDeclareTime > item.serverTime) {
        cardCase.isLastResultPending = true;
      }
    }
    
    if (cardCase.isNext && item.next.status == 'live') {
      cardCase.isNextLive = true;
      if(item.next.isResume){
        var startTime = new Date(item.next.attempts[0].startTime).getTime();
        var duration = Number(item.next.duration) * 60 * 1000;
        var timeLeft = duration - (Number(item.serverTime) - startTime);
        var lastTime = startTime + timeLeft;
        remainingTime = new Date(startTime+duration);       
      }
      else {
        remainingTime = item.next.endDate;
      }     
    }
    else if(item.next){
      remainingTime = item.next.endDate;
    }

    cardCase.isMoreDetailsSecondaryBtn = cardCase.isNext && ((cardCase.isNext && cardCase.isNextLive && item.next.isBought) || (cardCase.isNext && !cardCase.isNextLive && item.next.isBought && (!item.next.isInterested && item.next.price=='NA')) || (cardCase.isNext && !item.next.isBought)) && (cardCase.isNext && !cardCase.isNextLive); 
    
    return {
        cardCase,
        remainingTime        
    }
  }

  createTestAttempt(test) {
    //adding attempt into single test attempt
    let attempts = [];
    let attempt = {}
    test.attempts.forEach(element => {
      attempt['id'] = element._id;
      attempt['score'] = element.score;
      attempt['totalScore'] = element.totalScore;
      attempt['correct'] = element.correct;
      attempt['incorrect'] = element.incorrect;
      attempt['totalTime'] = this.getDisplayTime(element.totalTime);
      attempt['startTime'] = element.startTime;
      attempt['improvement'] = element.improvement;
      attempt['rank'] = element.rank;
      attempt['rankPotential'] = element.rankPotential;
      attempts.push(attempt);
    })
    return attempts;
  }

 getDisplayTime(time) {
    let timeTaken = '';
    let hr = 0, min = 0, sec = 0;
    hr = Math.floor(time / (60 * 60));
    min = Math.floor((time - (hr * 60 * 60)) / 60);
    sec = Math.floor(time - (hr * 60 * 60) - (min * 60));
    if (hr > 0)
      timeTaken = timeTaken + hr + (hr <= 1 ? " Hr " : " Hrs ") + min + (min <= 1 ? " Min " : " Mins ") + sec + (sec <= 1 ? " Sec " : " Secs");
    else if (min > 0)
      timeTaken = timeTaken + min + (min <= 1 ? " Min " : " Mins ") + sec + (sec <= 1 ? " Sec " : " Secs");
    else
      timeTaken = timeTaken + sec + (sec <= 1 ? " Sec " : " Secs");

    return timeTaken;
  }

 updateCarousel(translateValue) {
    this.carouselNavigationStyle = {
      transform: `translateX(${translateValue})`
    }
 }
 handleLeftNavClick() {
    let translateValue = `-${67*(this.selectedNavIndex-1)}%`;
    this.showRightNav = true;
    this.selectedNavIndex = this.selectedNavIndex - 1;
    if(this.selectedNavIndex === 0) {
      this.showLeftNav = false;
    }
    this.updateCarousel(translateValue);    
 }
 handleRightNavClick() {
    let translateValue = `-${(67*(this.selectedNavIndex+1))}%`;
    this.showLeftNav = true;
    this.selectedNavIndex = this.selectedNavIndex + 1
    if(this.selectedNavIndex === this.liveTests.length-1) {
      let cardMargin = 1500/document.documentElement.clientWidth;
      console.log(cardMargin);
      translateValue = `calc(-${67*(this.selectedNavIndex)-34}% - 5px)`;
      console.log('translateValue', translateValue);
    }
    if(this.selectedNavIndex === this.liveTests.length-1) {
      this.showRightNav = false;
    }
    this.updateCarousel(translateValue);
 }
 viewResult(test) {
    localStorage.setItem("isResult", "1");
    localStorage.removeItem('testName');
    localStorage.setItem('testType', String(test.prev.type));
    localStorage.setItem('testName', test.prev.testName);
    localStorage.setItem('previousUrl', this.router.url)
    this.router.navigate(['/test/testResults/' + test.prev.testId + '/' + test.prev.attempts[0].id + '/' + test.prev.type]);
  }

  buyNow(test) {
    let packageInfo = { '_id': test.next.packageId };
    this.cleverTap.eventProductPurchase(test.next.price, test.next.testName, "Buy Now");
    const payloadProfile = {
      "Site": {
        "Course Name": test.next.testName
      }
    }
    ClevertapReact.profile(payloadProfile);

    localStorage.setItem("buyNowFlag", "true");
    this.store.dispatch(new CartActions.AddItemStart(packageInfo))
  }

  moreDetails(test) {
    if(test.cardCase.isPreJee) {
      this.router.navigate(['/pre-jee-main']);
    }
    else if(test.cardCase.isPreJeeAdv) {
      this.router.navigate(['/pre-jee-advanced']);
    }
    else if(test.cardCase.isPreBitsat) {
      this.router.navigate(['/pre-bitsat']);
    }
    else if(test.cardCase.isPreNtse) {
      this.router.navigate(['/pre-ntse-stage-2']);
    }
  }
  
  attemptTest(test) {
    let queryParam = '';
    sessionStorage.setItem('mockTest','true');
    sessionStorage.setItem('mockTestEndDate', test.next.endDate.toString());
    let name = test.next.testName.replace(/\//g,'');
    for(let i=0; i<20; i++) {
      name = name.replace("(", "");
      name = name.replace(")", "");
      name = name.replace("&", "and");
    }
    name = encodeURI(String(name));
    let testType = 'others';
    if(test.cardCase.isPreJee) {
      testType = 'jee-main';
    }
    else if(test.cardCase.isPreJeeAdv) {
      testType = 'jee-advanced';
    }
    if (test.next.isResume) {
      this.router.navigateByUrl('/test/test-ongoing/' + testType + '/' + name + '/' + test.next.testId + '/' + test.next.courseId + '/' + test.next.attempts[0].id + queryParam);
    }
    else {
      this.router.navigateByUrl('/test/test-ongoing/' + testType + '/' + name + '/' + test.next.testId + '/' + test.next.courseId + queryParam);
    }

  }
  updateTestList(test) {
    let tests = this.liveTests.map((item)=>{
      if(item.next.testId === test.next.testId) {
        return test;
      }
      return item;
    })
    this.liveTests = tests.map((item, index)=>{
      let cardCase = this.createCardCase(item);
      let testData = {
        prev: item.prev,
        next: item.next,
        ...cardCase,
        color: this.cardColors[index%3],
        serverTime: item.serverTime
      }
      return testData;
    })    
  }
  bookMySeat(test) {
    test.next.isInterested = true;
    this.updateTestList(test);
    $('#bookmysheet').modal('show');
    this.preJeeService.bookMySeat(test.next.packageId);
  }
  changeNexttestStatus(test, status) {
    test.next.status = status;   
    this.updateTestList(test);
    //this.getLiveTests(this.userId, this.classId); 
  }
  fetchTest() {
    this.getLiveTests(this.userId, this.classId);
  }
  ngOnDestroy(){
    if (this.subSubscription) {
      this.subSubscription.unsubscribe();
    }
  }
}
