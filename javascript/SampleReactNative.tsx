import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback, StyleSheet, Image, TextInput } from 'react-native';
import { dimens, colors } from '../../../config';
import { inject, observer } from 'mobx-react';
import { NewDashboardStore } from '../../../store';
import { heightPercentage, widthPercentage } from '../../../common';
import { hideModal } from '../../../services';
import { getUserId } from '../../../utils';

interface Props {
    newDashboardStore?: NewDashboardStore
}

interface State {
    dashboardName: string
    isValid: boolean
}

@inject('newDashboardStore')
@observer
export default class CreateDashboard extends Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            dashboardName: '',
            isValid: false
        };
    }

    renderHeader = () => {
        return (
            <View style={styles.header}>
                <TouchableWithoutFeedback style={styles.closeIcon} onPress={hideModal}>
                    <Image style={styles.closeIcon} source={require('../../../../images/cross.png')} resizeMode='contain' />
                </TouchableWithoutFeedback>
                <Text style={styles.headerText}>Create Dashboard</Text>
            </View>
        )
    }

    onCreatePress = async () => {
        const { newDashboardStore } = this.props;
        const userId = await getUserId();
        newDashboardStore.createDashboard(userId, this.state.dashboardName);
        hideModal();
    }

    _onTextChange = (text) => {
        this.setState({
            dashboardName: text,
            isValid: text && /^[a-zA-Z ]+$/.test(text)
        });
    }

    render() {
        const { isValid } = this.state;
        return (
            <TouchableWithoutFeedback>
                <View style={styles.wrapper}>
                    {this.renderHeader()}
                    <View style={styles.bottomContainer}>
                        <View style={styles.createContainer}>
                            <Image style={styles.asset} source={require('../../../../images/create_dashboard.png')} resizeMode='contain' />

                            <TextInput
                                placeholder={'Type your dashboard name'}
                                style={styles.dashboardInput}
                                onChangeText={this._onTextChange}
                                value={this.state.dashboardName}
                                underlineColorAndroid='transparent'
                                />
                        </View>
                        <TouchableWithoutFeedback style={styles.profileLink} onPress={isValid ? this.onCreatePress : null}>
                            <View>
                                <Text style={[styles.linkText, isValid ? styles.validColor: {} ]}>Create dashboard</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        borderRadius: 6,
        backgroundColor: colors.White,
        height: heightPercentage(35),
        width: widthPercentage(80)
    },
    header: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginHorizontal: 10,
    },
    closeIcon: {
        width: 15,
        height: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: dimens.size16,
        color: colors.SubHeaderColor,
        lineHeight: 24,
        marginLeft: 10
    },
    bottomContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    profileLink: {
        paddingVertical: 16,
        alignItems: 'center',
        borderColor: '#CBCBCB',
        borderTopWidth: 1,
        height: 50,
        width: '100%'
    },
    linkText: {
        fontSize: dimens.size13,
        lineHeight: 13,
        color: '#999999'
    },
    validColor: {
        color: '#1D7DEA'
    },
    createContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 20,
        width: '100%'
    },
    asset: {
        width: 92,
        height: 66
    },
    dashboardInput: {
        width: '75%',
        color: colors.MettalicGrey,
        marginLeft: 8,
        height: 40,
        textAlign: 'center'
    }
});